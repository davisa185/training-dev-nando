'use strict';

import template from 'modules/index/templates/IndexLayoutView.hbs';
import TrackingHelper from 'common/utility/TrackingHelper';

var clickEvent = (window.Modernizr.touch) ? 'touchend' : 'click';

export default Marionette.LayoutView.extend({
    get template() {
        return template;
    },
    get className() {
        return 'view-content';
    },
    onBeforeRender: function() {

    },
    onRender() {
        $('body').addClass('index');
    },
    onShow(){

    }




});
