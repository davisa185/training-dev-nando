'use strict';

import IndexLayoutView from 'modules/index/views/IndexLayoutView';


export default Marionette.Controller.extend({
    initialize(options) {
        this.options = options;
        this.indexLayoutView = new IndexLayoutView();

    },
    index() {
        //this.options.container.show(this.indexLayoutView);


    },
    openSweeps() {
      this.router.navigate('sweeps', {
          trigger: true
      });
    }
});
