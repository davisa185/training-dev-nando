'use strict';

import template from 'modules/application/templates/applicationLayoutView.hbs';
import Device from 'common/vendor/device.min';


export default Marionette.LayoutView.extend({
  get el() {
    return '#application';
  },
  get template() {
    return template;
  },
  regions() {
    return {
      mainContent: '#main-content'
    };
  },

  initialize() {
      /*if (Foundation.utils.is_medium_up()) {
          $(window).on('resize', this.resize.bind(this));
      }*/


      this.radioChannel = Radio.channel('steps');
      this.resize();

      $(window).on('resize', this.resize.bind(this) );


      //$(window).on('orientationchange', this.resize.bind(this));
  },

  checkEnviroment() {

    window.onInput = false;
    window.sufix = '';

    if ( device.mobile() ) {
      window.plat = 'mobile';
    } else if ( device.tablet() ){
      window.plat = 'tablet';
    } else {
      window.plat = 'desktop';
    }

    if ( device.ipad() ){
      window.ipad = true;
    } else {
      window.ipad = false;
    }

    if ( device.landscape() ) {
      window.orient = 'landscape';
    }else {
      window.orient = 'portrait';
    }



    if ( window.plat == 'mobile' || ( window.plat == 'tablet' && window.orient == 'portrait' ) ) {
      window.sufix = 'Mob';
    }

  },


  resize() {
      //console.log('comicTablet:', window.comicTablet);
      if ( window.onInput ) {
        return;
      }

      console.log('resize FUNC. enter');

      this.checkEnviroment();


      console.log ( $("html").css("font-size") );

      if ( window.ipad && window.orient == 'portrait' ) {
        console.log('stop a todo...');
        $("html").css("font-size", '100%');
        return;
      }

      if ( window.plat == 'desktop' || ( window.plat == 'tablet' && window.orient == 'landscape' ) ){

        console.log('ajustando');

        var displayHeight = $(window).height();
        var displayWidth = $(window).width();
        //Standard dimensions, for which the body font size is correct

        var preferredWidth = 1162;
        var preferredHeight = 942;

        if (displayHeight < preferredHeight || displayWidth < preferredWidth) {
            var heightPercentage = (displayHeight * 100) / preferredHeight;
            var widthPercentage = (displayWidth * 100) / preferredWidth;
            var percentage = Math.min(heightPercentage, widthPercentage);
            var newFontSize = percentage.toFixed(2);
            $("html").css("font-size", newFontSize + '%');
        } else {
            $("html").css("font-size", '100%');
        }

      } else {
        console.log('no hay ajuste');
      }

  }

});
