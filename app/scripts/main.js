'use strict';

import Application from 'modules/application/ApplicationModule';
import IndexModule from 'modules/index/IndexModule';


let app = new Application();



app.index = new IndexModule({
    container: app.layout.mainContent
});


app.start();
