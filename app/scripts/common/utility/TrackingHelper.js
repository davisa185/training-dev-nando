'use strict';

import DetectDevice from 'common/utility/detect-device';

export default {

    trackingLink: function(e) {
        this.TrackClick( $(e.currentTarget).attr('data-track'), $(e.currentTarget).attr('data-section') );
    },

    linkOut: function(e) {
        window.open($(e.currentTarget).attr('data-link'),$(e.currentTarget).attr('target'));
    },

    TrackPageView: function(page_name) {
        this._NickTrack('uor_' + page_name + '_view_' + DetectDevice.getDeviceType().platform.toLowerCase());
    },

    TrackEvent: function( name ) {
        this._NickTrack( 'uor_'+ name +'_event_' + DetectDevice.getDeviceType().platform.toLowerCase() );
    },


    TrackClick: function( click_name ) {
        this._NickTrack( 'uor_' + click_name + "_click_" + DetectDevice.getDeviceType().platform.toLowerCase() );
    },

    _NickTrack: function(value) {
        if ( typeof pageNameAppend === "undefined" ) {
            console.log('Console: pageNameAppend(' + value + ')');
        } else {
            console.log ( value );
            pageNameAppend( value );
        }
    }


}
