'use strict';

export default {
    getDeviceType() {
        var isiOS = (navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false),
            isDevice = (navigator.userAgent.match(/(iPad|iPhone|iPod|Android|IEMobile)/g) ? true : false);

        var info = {
            orientation: 'landscape',
            ismobileSize: false,
            widthSize: 400,
            platform: 'DESKTOP',
            isiOS: false,
            isDevice:false
        };

        var resolution = {
            mobileWidth: 767
        };



        if (isDevice) { //FOR MOBILE AND TABLET
          info.isDevice = true;
            if (isiOS) {
                info.isiOS = true;
                var mql = window.matchMedia("(orientation: portrait)");
                if (mql.matches) {
                    info.orientation = 'portrait';
                    info.widthSize = screen.width;
                } else {
                    info.orientation = 'landscape';
                    info.widthSize = screen.height;
                }
                info.ismobileSize = (screen.width <= resolution.mobileWidth) ? true : false;

            } else {
                info.isiOS = false;
                info.widthSize = screen.width;
                if (screen.height > screen.width) {
                    info.orientation = 'portrait';
                    info.ismobileSize = (screen.width <= resolution.mobileWidth) ? true : false;
                } else {
                    info.orientation = 'landscape';
                    info.ismobileSize = (screen.height <= resolution.mobileWidth) ? true : false;
                }
            }
            info.platform = (info.ismobileSize) ? "MOBILE" : "TABLET";
        } else { //FOR DESKTOP

            info.widthSize = window.innerWidth;
            if (window.innerWidth < window.innerHeight) {
                info.orientation = 'portrait';
                info.ismobileSize = (window.innerWidth <= resolution.mobileWidth) ? true : false;
            } else {
                info.orientation = 'landscape';
                info.ismobileSize = (window.innerHeight <= resolution.mobileWidth) ? true : false;
            }
        }

        return info;
    }
}
