<?php
  date_default_timezone_set('America/New_York');
  $dateTimePost = '5 May 2016 11:59:59';

    if(strtotime('now') < strtotime($dateTimePost)){   //22 february 2016 11:59:59
      $isOver = false;
    }else{
      $isOver = true;
    }
    header('Content-Type: application/json');
    echo json_encode(array(
        "isOver" => $isOver
    ));
?>
