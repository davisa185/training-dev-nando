(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 660,
	height: 172,
	fps: 30,
	color: "#FFFFFF",
	manifest: []
};



// symbols:



(lib.zigzagarrow_1 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#546169").s().p("AwNAjIg2gNIhQgSQgwgKgggFIh4AbQhIARgwAFQgdgCg4gNQg4gOgdgBQgQgBghgHQgggHgRgBQgSAEhcAVQhDARgsACQgpgDhUgVQhRgTgrgCQgxAIhMAOIh+AZQgQADgZgGQgggHgIgBIgFgQIAsAIQAbAFARgEQBxgVCRgbQApAJBQATIB4AZIBugZQBBgPAsgIQAjADAvAIIBRAOQBhAdBEgTQBXgWBngPQApAGBQATQBQAQAoAGQBYgPCkghQASgDAVAEIAnAKQAbAIAxAIQA3AKAVAGIAXAGQAOADALgDIBHgSIBGgQQAUgCAngJQAjgGAYAJQCOAcAzAOQAtgHBEgQIB0gYQAwAFBRASQBfASAjAFQAugEBOgSQBZgWAkgFQAZADAxANQAxALAaADQAVADAsANQAnAJAagKIBvgVQBFgPAqgHQAyABBPAUQBjAXAfAEQAtgHBGgQIBygaQArAEA/APIBpAXQAeAKAtgKIBJgQQAVgDAogKQAogLAUgCIBvAOQBDAJArABQAMgDAOgMIAWgWQAwALALAEQAhALATASQgWAWghAHQgbAGgkgCIghgXQgnABhPgOQhOgLgoABIh0AZQhGAQguAFQgngFhMgUQhMgRgngFQgpAGhPARQhQATgoAGQgtgIhQgRQhWgRgngHIihAeIg8AOQgjAHgZgEIh2gcQhJgPgvgDQggAJg9ALQhBAOgdAIQgpAJglgIQgpgFhKgRQhOgPgkgFQgpAHhDAOIhrAYQgtAAhQgVQhZgWglgEQgiAFhVAUQhLAUgtADQgtgIhLgQQhbgTgdgFIh+AWQhOAOgvAMIgQACQgSAAgWgFg");
	this.shape.setTransform(343.6,4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#546169").s().p("AABAgIj6guQgiACg6AJQhDAKgZACQAagJAogJIBDgOQAvgMA4AMQAhAJBCALQBDAJAfAJQAyACBVgUQBhgaAmgDIBSASIBSAQIgOAQIhLgSQgwgKgbgGQgaACgqAKQg2AMgOADIhCAPQgcAHgWAAIgRgBg");
	this.shape_1.setTransform(43.4,4.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,600.3,8);


(lib.zigzag_Arrow02 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#546169").s().p("A1xAnQgVAFgYgNQgHgEgfgZQAzglA/gEIAbAXQARANAPABQBAgKB/gPQAuAFBKATQBaAUAdAFIBzgaQBFgRAugGQAjAGBWAUQBNAQAsAGQAfgFBZgVQBLgTAtgFQAsAHBLARQBUARAhAGQAiAFAvgMQAagGA0gMIA8gPQAigJAaAGQBGAPCPAgQAxgIBWgQICGgbIByAcQBBANAyAIQAsgGBlgTQBdgTA0gGIA9ASQApALAUAFQgQAAhOgKQg5gJgjAMIhwAVQhGAOgqAHQgsgJhIgRIhzgYQgsAFhHAPQhWAUgbAFQgvAIhBgSQhYgYhKgMIiyAmIgiAKQgWAFgOgEQgYgGg0gJQgwgIgZgHQg0gLgbgEQgvgHggANQgYAHgoAHQgyAJgPAEIgkAKQgVAFgQgEIhtgaQhCgNgtgJQgjAGhUATQhMAUgsAEIgzgMQgggIgSgDIhKgQQgsgKggADQgbADhDAIQg8AKgjACQgVAOgMAGQgNAGgNAAIgMgBg");
	this.shape.setTransform(147.8,4.1);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,295.5,8.2);


(lib.tm = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F37137").s().p("AgGgBQABgYgUgFIAzAAQgXAEAAAXQABAcgEAGQgHgGABgag");
	this.shape.setTransform(2.6,3.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F37137").s().p("AghACIADghQAQASAOgBQAOgCAQgSQAHAkgHAgQgJgFAAgOQAAgPgEgCQgLAGgGACQgIADgGgOQgFAFgBAOQgBAPgJAHQgEgPABgTg");
	this.shape_1.setTransform(9.9,3.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,13.3,7.2);


(lib.E = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00B4E2").s().p("AhcDTQgBkmgHiBQBEgDCFADIgEBOQgXgFgfgCIg1AAQAEAXAAAhIAAA4QApgBA3ADIgJBSQgWAEgcACIgxACIAIBGQA3ACAjgIIAFBYg");
	this.shape.setTransform(10.1,21.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,20.2,43);


(lib.S = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00B4E2").s().p("Ag/DGQgdgMgTgDIADg/QACgkgCgaIBSAFIADBFIAvAFQAEggAAgQQAAgcgKgSIg4gYQgigOgZgFQgLg7gBgeQgDgzAPglQArgmBAACIAtARQAcAKASAFQgEBAABAnIhJABIgBg9Ig1gBIgEBCQASAUAlAOQA1ATAKAGIAVA3QAKAhgGAZQgDAPgBAfQgDAagQANQgPATghAGQgnADgPACQgOgDgigNg");
	this.shape.setTransform(11.2,21.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,22.5,43);


(lib.R = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00B4E2").s().p("AgiA0IAAhxQAKABAWgBQARABALAJQADAJABAMIABAVIgOAQIASA2QgUAAgJgXQgNghgDgEQgEAbgCAigAgPgJIAcgDQgBgZgWgLIgFAng");
	this.shape.setTransform(3.6,6.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,7.1,12.5);


(lib.OrlandoR = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00B4E2").s().p("AgMAOIABgPIAAgMQACgHAJABQAKAAACAHQACAFgDAJQAAAMgBACQgBACgFABIgHABQgHAAgCgGg");
	this.shape.setTransform(4.5,4.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00B4E2").s().p("AgLAqQgcgJgDgbQgCgMACgIQAEgLAOgDIgBAAIABABIgHAYQgCAOAJALQAIALAQAAQAOABAKgLQAKgKgBgOQAAgOgMgKQgLgKgNABIgVAGIAAAAQAXgZAYARQAOAJAFAQQAFAOgHAPQgHAPgQAHQgJAEgIAAQgFAAgGgCg");
	this.shape_1.setTransform(4.5,4.5);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,8.9,8.9);


(lib.o = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00B4E2").s().p("AgcAxQgNg2AMgmQAJgVAWACQAZACAGAWQAEArgCAOQgEAhgcALgAgOgKIADAkQAAARANgEQAQgDgGgPQACgYAAgOQgBgYgPgEQgLANgBAWg");
	this.shape.setTransform(3.6,6.3);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,7.3,12.7);


(lib.o_1 = function() {
	this.initialize();

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF6700").s().p("AhXB1Qg7gsABhDQgFgsAcgqQAbgpAtgQQA/gaBBApQBBAoADBGQAGAzghAuQghAtgzANQgTAFgRAAQgsAAgqgfgAgTg6QgUAbABAjQABAnAZAWQAYALAPgdQANgYgCgZQADgbgTgYQgLgPgLAAQgIAAgLAKg");
	this.shape_1.setTransform(14.8,14.9);

	this.addChild(this.shape_1);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,29.5,29.8);


(lib.n = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6700").s().p("AAkgHQgBgTgMgNQgOgPgQAEQgNAHgIAMQgIAMAAANQgBAwABBkIhtAAIAAhTQAAg0ACgdQAKhBA/giQA+giA7AZQAqAQAbAlQAcAmgCApIABCLIhuABQABhlgCgwg");
	this.shape.setTransform(14.7,14.2);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,29.4,28.5);


(lib.e = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6700").s().p("Ag0CHQg6gXgXg+QgWg9Adg2QAbg8BGgRQBDgRA0ApQAeAUAOAmQALAhgCAnQh3gBg5ACIAKAdQAHARAMAGQAOAFAOgMQAIgIAOgSIBgABQgJBBhAAhQgiARggAAQgbAAgcgNgAgQg4IgRAeQAkADAmgDIgMgYQgIgNgLgGIgEgBQgMAAgKAOg");
	this.shape.setTransform(14.4,14.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,28.8,29.8);


(lib.lance = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#636E75").s().p("AgCAOQgNAAgDgJQAAgFAFAAIAHgCIAHgJQAFgFAHAGQAIAHgDAFQgHAMgLAAIgCAAg");
	this.shape.setTransform(27.3,17.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#636E75").s().p("AAAAXQgNgNACgOQABgEADgFIAGgKQAFADADAGIADAJQAFAPgIALQgBACgEABIgBAAIgBgBg");
	this.shape_1.setTransform(34,10.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#636E75").s().p("AAGAYQgLgKgOgVQgFgHALgJQAGgFAGAFQAPAPAHAVQgDAOgGAAQgDAAgDgDg");
	this.shape_2.setTransform(32.2,16);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#636E75").s().p("AgEAlQgGgCgCgEQgDgEACgFQAFgJABgNIAAgVQABgOAHgBQAIgBACAFQABADABAJIABANQABAHgEAKQgFAOgBAFQAAAEgDADQgCABgBAAIgDAAg");
	this.shape_3.setTransform(46.2,14);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#636E75").s().p("AgGAiQgKgDAEgKIADgKIAAgUQgBgLAAgFQABgJAJAAQAJgBACAJIACAPQADAJgEAKIgIATQgDAIgDAAIgEgBg");
	this.shape_4.setTransform(42.4,13.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#636E75").s().p("AgJAiQgIgEADgKIAGgTQADgJgBgJQgCgQAKgBQAHAAADAFQACADABAIIABAJQgDAYgIAMQgFAIgDAAIgGgBg");
	this.shape_5.setTransform(38.1,12.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#636E75").s().p("AAHAkQgCgBgEgIIgYgwQgHgOAPgDIAFAEIAhAxQAEAHgBAGQgBAGgHAEIgFABQgDAAgDgDg");
	this.shape_6.setTransform(25.4,3.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#636E75").s().p("Ag6A/QgGgHAFgFIALgHQA7gkAag7QACgGAHgHQAFgEAHAFQAEACAAAGIgWAvQgOAbgYAJQgHADgLALIgTARQgIAGgDABIgEABQgEAAgEgEg");
	this.shape_7.setTransform(27,11.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#636E75").s().p("AAqA2QgQgJgbgGIgvgMQgUgGgTgRIgjgfQAwgQAagGQAmgJAggCQAjgBAUAKQAXALANAfQALAagBAEQgBAHgTARQgQANgMADIgGAAQgMAAgPgHg");
	this.shape_8.setTransform(12.4,7.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#636E75").s().p("AAAAWQiYgWABgaIAAgCQAAgWCSAbQBGAOBJAQIAOAfQhMgFhMgLg");
	this.shape_9.setTransform(65.3,16.9);

	this.addChild(this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,80.6,20.8);


(lib.Y = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6700").s().p("AhWCwIAChBQA8AIApADIADglQg3gEgNgDQgggggOgeQgBgTADhLQADg8gFglIBBgCIAGC8IArABQgCghABg1IABhYIBMgDQgIA3AABXQAABwgCAcQgEANgLASIgSAeQhbgCgwAAg");
	this.shape.setTransform(9.6,17.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,19.2,35.7);


(lib.w = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6700").s().p("AhcC2QgEhIgOhqIgXiwIBRgDQgDB4ATBXQAVhQAPh2IAlgCIAHBfQAEA5AFAjQALgmAJg9IAPhnIAuAAQgHAygXCAQgUBtgIBCIgvgBIgRiPQgYA/gCBfg");
	this.shape.setTransform(13.5,18.5);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,27,36.9);


(lib.A = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6700").s().p("AhnCxQAIg5AukoQAyAEA7gFQAWDVAWCIIhOABQgJhEgBgZIgXACQgOABgKADQgIAnAIA1gAgQg0IgLA/IAngKIgGg7QgEgjgGgYQgGAZgGAog");
	this.shape.setTransform(10.5,17.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,20.9,35.7);


(lib.feather02 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#636E75").s().p("AgJAUQgCgPgNguQAOAQAMAZIAXAqg");
	this.shape.setTransform(14,4.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#636E75").s().p("AgKAAQABgMADgVIAFgjQAMAdAAAnQAAANgGA4QgMgxgDgUg");
	this.shape_1.setTransform(10,7.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#636E75").s().p("AAqg1QgFAfgcAcIgyAwQAihLAxggg");
	this.shape_2.setTransform(4.3,5.9);

	this.addChild(this.shape_2,this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,16.6,14.7);


(lib.feather01 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#636E75").s().p("AgSgtQAYAkAEAHQAOAYgIAXQgag3gIgjg");
	this.shape.setTransform(2,6.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#636E75").s().p("AgFAFQgcgdgKgeQA2AiAhBLIgxgyg");
	this.shape_1.setTransform(6,5.6);

	this.addChild(this.shape_1,this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,10.5,11.1);


(lib.Arrow_Back02 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#546169").s().p("AA1AiIhPggIg5gTQgigMgWgLIgLgRQAoANBuApQBdAhA6AQIgPAMQgigEgxgUg");
	this.shape.setTransform(15.1,5.9);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,30.2,11.8);


(lib.arrow_Back1 = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#546169").s().p("AhqAjIAggMIBngmQBCgaAogNIgJASQgeAOgxARIhOAcIgyAUQgeALgXABQAJgNATgHg");
	this.shape.setTransform(13.6,5.6);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,27.2,11.1);


(lib.A_1 = function() {
	this.initialize();

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00B4E2").s().p("AgIg/IASACQAXBKAJAxIgUgMIgUgOQgIABgNALQgQANgGACQAKg3AXhHgAgMARIAZAAIgMgsg");
	this.shape_1.setTransform(4.2,6.4);

	this.addChild(this.shape_1);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,8.5,12.8);


(lib.circle_gradient = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["rgba(255,255,255,0.8)","rgba(255,255,255,0)"],[0.6,0.953],0,0,0,0,0,31.1).s().p("AjZDZQhahaAAh/QAAh+BahbQBbhaB+AAQB/AABaBaQBbBbgBB+QABB/hbBaQhaBbh/gBQh+ABhbhbg");
	this.shape.setTransform(30.8,30.8);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,61.6,61.6);


(lib.With = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.w();
	this.instance.setTransform(6.5,9,0.483,0.483,0,0,0,13.4,18.6);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6700").s().p("AgMBPQgCgdAAg0IgBhSIAfACIgFCng");
	this.shape.setTransform(16.4,9.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF6700").s().p("AgkBSQgChPAChOIAdAAIABBDIAUgCIAChJIAVADQgBAgAAAwIACBOIgaAAIACg6QgUACgBAXQAAAMABAbg");
	this.shape_1.setTransform(34.3,8.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF6700").s().p("AgQgtQgOgBgFgLQgDgGgBgRIBMABIADATIgcAGQgCBTAJAzIgkABg");
	this.shape_2.setTransform(24.2,9);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,38.1,17.9);


(lib.touniversal_Txt = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.OrlandoR();
	this.instance.setTransform(223.3,2.7,0.471,0.471,0,0,0,4.5,4.5);

	this.instance_1 = new lib.A_1();
	this.instance_1.setTransform(179,7,1,1,0,0,0,4.2,6.4);

	this.instance_2 = new lib.A_1();
	this.instance_2.setTransform(113.5,7,1,1,0,0,0,4.2,6.4);

	this.instance_3 = new lib.R();
	this.instance_3.setTransform(286.8,7,1,1,0,0,0,3.6,6.3);

	this.instance_4 = new lib.R();
	this.instance_4.setTransform(239.6,7,1,1,0,0,0,3.6,6.3);

	this.instance_5 = new lib.R();
	this.instance_5.setTransform(155.5,7,1,1,0,0,0,3.6,6.3);

	this.instance_6 = new lib.R();
	this.instance_6.setTransform(89.6,7,1,1,0,0,0,3.6,6.3);

	this.instance_7 = new lib.o();
	this.instance_7.setTransform(274.1,7,1,1,0,0,0,3.6,6.4);

	this.instance_8 = new lib.o();
	this.instance_8.setTransform(216.4,7,1,1,0,0,0,3.6,6.4);

	this.instance_9 = new lib.o();
	this.instance_9.setTransform(142.9,7,1,1,0,0,0,3.6,6.4);

	this.instance_10 = new lib.o();
	this.instance_10.setTransform(14.7,7,1,1,0,0,0,3.6,6.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00B4E2").s().p("AgJALQAAgfgCgTIgcgOQAQgJAXAAIAoACQgNAOgRAJIAAArQAAAdgBARIgRALQgBgQAAgkg");
	this.shape.setTransform(297.5,7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00B4E2").s().p("AgXAxQgLgJgBgOQAQABAcANQAMgTgSgNQgXgLgIgIQgLgTAOgTQAOgTATAHQANgBAHAOIAHAYIgogXIgEAZQAIAGAUAKQARAJgBAQQAEARgNAMQgJAIgSAIIgWgPg");
	this.shape_1.setTransform(262.1,6.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00B4E2").s().p("AAGAlQgJgagIgLQgCAaAAAhIgVAAQgBhSAEgqQAOAFAJARQAFAJAGAWIAMADQgDgYAAgMQABgTAVABQABBVgBAsQgQgHgMgWg");
	this.shape_2.setTransform(191.6,6.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00B4E2").s().p("AgaA9IAAh5IAVAGQABAUAAAdIAAAuIAfACIAAASg");
	this.shape_3.setTransform(125.5,6.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00B4E2").s().p("AgQA4QgPgKgHgTQAEgBAaAKQARAHAHgNQAAgPgYgOQgZgKADgSQgCgPANgLQANgLANAEQAMgBAIANQAFAHAHAQQgSgEgbgPQgJATAQALIAgARQADAXgBAJQgDARgPAFQgIAFgIAAQgHAAgKgGg");
	this.shape_4.setTransform(101.2,6.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#00B4E2").s().p("AgeA7IAAh2IA2AAIgCATIgeAAIgBAeIAdACIgBAPIgbABIgBAeIAoAHQgIAPgUAAQgWgCgLABg");
	this.shape_5.setTransform(78.2,6.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00B4E2").s().p("Agmg9IAXAGIAHAqQAEAXAEAPIAHgnIAJgpIAYgGIgdB5IgVACQgPg9gNg+g");
	this.shape_6.setTransform(66.3,6.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#00B4E2").s().p("AgJA8IAAg8QgBglADgWIAPAAQADAogBBPg");
	this.shape_7.setTransform(56.3,6.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#00B4E2").s().p("AAGAlQgPgggEgEIABAnQAAAZgUgDQgDg5ADhGQAQAIALAWIARAjIABgnQgBgXAWABQABBUgCArQgQgHgLgWg");
	this.shape_8.setTransform(46,6.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#00B4E2").s().p("AggAqQgEgmADg6IAVgHQABAiAABEIAXAAQABgqgBg5IAXABIAAAyQAAAcgCAVQgLAUgWAAQgVAAgLgUg");
	this.shape_9.setTransform(33.4,6.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#00B4E2").s().p("AgdA8IAAh4QALACAWgBQAUABAGAQIglACIgBAeIAbADIACANIgcADIgCAdIAnAFQgGAQgUACQgWgDgLACg");
	this.shape_10.setTransform(251.4,6.7);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#00B4E2").s().p("AggAAIAAg7QALACAWgBQAUADAIANQAKAggIAxQgFASgXACIglABIACg8gAgKAuQAKgEAMgJQADgfgBgNQgCgbgVgGQgFAmAEA0g");
	this.shape_11.setTransform(204.3,6.6);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#00B4E2").s().p("Agbg6IAWAAIgBBhQAGACASAAQAOADgFAPIg3ABQACg2gBhAg");
	this.shape_12.setTransform(167.2,6.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#00B4E2").s().p("AgLA7IABgwQAAgagCgUIgWgKIACgNQAmgBAdACQgJAPgRAIIAABeg");
	this.shape_13.setTransform(3.6,6.7);

	this.addChild(this.shape_13,this.shape_12,this.shape_11,this.shape_10,this.shape_9,this.shape_8,this.shape_7,this.shape_6,this.shape_5,this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance_10,this.instance_9,this.instance_8,this.instance_7,this.instance_6,this.instance_5,this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,301.6,13.4);


(lib.sweepstakes_logo = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.w();
	this.instance.setTransform(40.9,22.5,1.192,1.192,0,0,0,13.5,18.5);
	this.instance.filters = [new cjs.ColorFilter(0, 0, 0, 1, 0, 180, 226, 0)];
	this.instance.cache(-2,-2,31,41);

	this.instance_1 = new lib.S();
	this.instance_1.setTransform(263.9,22.3,1,1,0,0,0,11.2,21.4);

	this.instance_2 = new lib.S();
	this.instance_2.setTransform(142.4,22.3,1,1,0,0,0,11.2,21.4);

	this.instance_3 = new lib.S();
	this.instance_3.setTransform(11.2,22.3,1,1,0,0,0,11.2,21.4);

	this.instance_4 = new lib.E();
	this.instance_4.setTransform(239.6,22.4,1,1,0,0,0,10.1,21.5);

	this.instance_5 = new lib.E();
	this.instance_5.setTransform(93.6,22.4,1,1,0,0,0,10.1,21.5);

	this.instance_6 = new lib.E();
	this.instance_6.setTransform(69.8,22.6,1,1,0,0,0,10.1,21.5);

	this.instance_7 = new lib.A();
	this.instance_7.setTransform(188.1,22.9,1.215,1.215,0,0,0,10.4,17.8);
	this.instance_7.filters = [new cjs.ColorFilter(0, 0, 0, 1, 0, 180, 226, 0)];
	this.instance_7.cache(-2,-2,25,40);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00B4E2").s().p("AgviBIgyAGQAAgmgDgwQAiAABCADQA8ACApgGIgCBGIg/gDIgEBDQgCAnADAcQACALAEBhQADBCAMAqQhCgBgnAFQAEjiAAhyg");
	this.shape.setTransform(165.6,22.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00B4E2").s().p("AhjADQgDiKAAhFIBGgDQApgBAagEIA+A+QACAaAEAmIAHBAQgEAdgOAXQgPAbgZANQgXAJgpABQACAsgCArIACApQhCACghADQALhUgBh+gAgRiKIAACOIAugDQgEgdAAgpIADhHIgtACg");
	this.shape_1.setTransform(117.8,22.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00B4E2").s().p("AAKCUQgDgsAFgbIgsgkIAABTQAAAwADAiIhSACQAEhQABiEQABihACg2IBNAAIgICyQAthGAfheQAOgDAVABIAjACIgCA/QAAAmgHAXQgrAqglAvIArAgQAZAUAQAPQACAYAAAuQAAAwACAWIhhAFIgEhIg");
	this.shape_2.setTransform(214.6,22);

	this.addChild(this.shape_2,this.shape_1,this.shape,this.instance_7,this.instance_6,this.instance_5,this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,275.2,44.5);


(lib.nick_logo = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.tm();
	this.instance.setTransform(307,40.9,0.593,0.593,0,0,0,6.7,3.6);

	this.instance_1 = new lib.o_1();
	this.instance_1.setTransform(254.5,29.4,1,1,0,0,0,14.8,14.9);

	this.instance_2 = new lib.o_1();
	this.instance_2.setTransform(161.4,29.4,1,1,0,0,0,14.8,14.9);

	this.instance_3 = new lib.e();
	this.instance_3.setTransform(223.4,29.4,1,1,0,0,0,14.4,14.9);

	this.instance_4 = new lib.e();
	this.instance_4.setTransform(117.4,29.4,1,1,0,0,0,14.4,14.9);

	this.instance_5 = new lib.n();
	this.instance_5.setTransform(285.6,28.7,1,1,0,0,0,14.7,14.2);

	this.instance_6 = new lib.n();
	this.instance_6.setTransform(14.7,28.7,1,1,0,0,0,14.7,14.2);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6700").s().p("AiMCPIABh3QgBhLABgqQADgmAqgHQAqgGARAhQAFAQAAAbQgBAjABAJQANgPATgoQAUgkAQgPQAdgXAhAWQAiAWgJAjQgGASgTAfQgTAdgGARQAHALAcAoQAWAfAJAWIgLAOQgSAEgfAAIgyAAIgeguQgRgcgNgQIgCAtIAAAtQgkgBhJABg");
	this.shape.setTransform(90.4,28.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF6700").s().p("Ag3CHQg4gWgYg8QgYg6Aag2QAXg6BAgVQA+gWA1AfQAfAQAVAgQATAcAIAlIhtABQgLgdgEgHQgMgUgRACQggAVgBAsQgBApAbAbQATALAPgVQAIgLAKgdIAyAAIAyABIACAUQgOBAg/AgQgiARgfAAQgcAAgbgNg");
	this.shape_1.setTransform(59.5,29.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF6700").s().p("AhXC/Qg2gmgEhIQgEhJAygqQAugvBbAKIgBg2QgBggACgVQACgQAPgLQAJgFAUgJIAQAAQAWAKAJAIQAQAOgBARQABBNgBCYQABBCgfAlQgjAvg+AJQgMACgKAAQgvAAglgdgAgTANQgVAeACAnQADAqAfAQQAYABAKgeQAIgVgBgeQABgagTgUQgKgLgJAAQgJAAgKAKg");
	this.shape_2.setTransform(192.5,22.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF6700").s().p("Ag1DWQgBj7ACh+QgCgRAOgOQAJgIAUgLIAYAAQATAKAJAKQANAOAAAQIAAF5IhrAAg");
	this.shape_3.setTransform(139.1,21.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF6700").s().p("Ag2ByQAAg+ABgmQAAgTASgWIAdglIgkgtQgWgbAFgZQADgjAwgRIASAAQAWAIANAOQAQARAAAVQACAXgWAYQgeAlgDAFQAIANAVAYQARAXgBATQACBCgBCFIhsAAIAAhkg");
	this.shape_4.setTransform(37.3,21.5);

	this.addChild(this.shape_4,this.shape_3,this.shape_2,this.shape_1,this.shape,this.instance_6,this.instance_5,this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,311,44.3);


(lib.fly_Away = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.w();
	this.instance.setTransform(106.2,18.6,1,1,0,0,0,13.5,18.4);

	this.instance_1 = new lib.A();
	this.instance_1.setTransform(130.7,18.7,1,1,0,0,0,10.5,17.8);

	this.instance_2 = new lib.A();
	this.instance_2.setTransform(81,18.7,1,1,0,0,0,10.5,17.8);

	this.instance_3 = new lib.Y();
	this.instance_3.setTransform(153.1,19.3,1,1,0,0,0,9.6,17.8);

	this.instance_4 = new lib.Y();
	this.instance_4.setTransform(44,17.8,1,1,0,0,0,9.6,17.8);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF6700").s().p("AhDiuIBJgCIgGEcIBEgDQgDAlgBAeQgrABhVAEQgFi+ACihg");
	this.shape.setTransform(25.7,18.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF6700").s().p("AhOCwQAJicgLjAIBQAAQAwAAAhgDIgBA8QgzgFgmACIgDAxQAnACAnABQgIA0gBAYIhDgCQAFAaACA7QADA3AEAcg");
	this.shape_1.setTransform(8.1,17.9);

	this.addChild(this.shape_1,this.shape,this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,162.7,37.1);


// stage content:
(lib.Sweeps_UO_Site = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_29 = function() {
		/* Stop at This Frame
		The  timeline will stop/pause at the frame where you insert this code.
		Can also be used to stop/pause the timeline of movieclips.
		*/
		
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(29).call(this.frame_29).wait(1));

	// touniversal_Txt
	this.instance = new lib.touniversal_Txt();
	this.instance.setTransform(478.6,93.7,1,1,0,0,0,150.8,6.7);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(15).to({_off:false},0).to({y:112,alpha:1},14,cjs.Ease.get(1)).wait(1));

	// sweepstakes_logo
	this.instance_1 = new lib.sweepstakes_logo();
	this.instance_1.setTransform(141.6,120.3,1,1,0,0,0,137.6,22.3);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(11).to({_off:false},0).to({x:166.9,alpha:1},14,cjs.Ease.get(1)).wait(5));

	// nick_logo
	this.instance_2 = new lib.nick_logo();
	this.instance_2.setTransform(506.2,48.3,1,1,0,0,0,150.2,22.1);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(8).to({_off:false},0).to({x:476,alpha:1},14,cjs.Ease.get(1)).wait(8));

	// With
	this.instance_3 = new lib.With();
	this.instance_3.setTransform(260.5,54.9,0.496,0.496,0,0,0,19.1,8.8);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(4).to({_off:false},0).to({regY:8.9,scaleX:1,scaleY:1,alpha:1},13,cjs.Ease.get(1)).wait(13));

	// fly_Away
	this.instance_4 = new lib.fly_Away();
	this.instance_4.setTransform(112.4,64.6,1,1,0,0,0,81.3,18.6);
	this.instance_4.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({y:52.5,alpha:1},14,cjs.Ease.get(1)).wait(16));

	// zigzag_Arrow02
	this.instance_5 = new lib.zigzag_Arrow02();
	this.instance_5.setTransform(508.8,137.5,1,1,0,0,0,147.8,4.1);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off:false},0).to({x:475.1,alpha:1},13,cjs.Ease.get(1)).wait(13));

	// zigzagarrow_1
	this.instance_6 = new lib.zigzagarrow_1();
	this.instance_6.setTransform(276.2,85.5,1,1,0,0,0,300.2,4);
	this.instance_6.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({x:327.4,alpha:1},14,cjs.Ease.get(1)).wait(16));

	// Layer 44 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("ApXDaIAAmvIGLAAIAAGvgADNDWIAAmvIGLAAIAAGvg");
	mask.setTransform(260.8,51.4);

	// Arrow_Back02
	this.instance_7 = new lib.Arrow_Back02();
	this.instance_7.setTransform(280.6,61.4,1,1,0,0,0,15.1,5.9);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.instance_7.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(14).to({_off:false},0).to({x:298.2,y:68.1,alpha:1},12,cjs.Ease.get(1)).wait(4));

	// arrow_Back1
	this.instance_8 = new lib.arrow_Back1();
	this.instance_8.setTransform(243.5,61,1,1,0,0,0,13.6,5.5);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.instance_8.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(12).to({_off:false},0).to({x:226.7,y:67.6,alpha:1},12,cjs.Ease.get(1)).wait(6));

	// feather02
	this.instance_9 = new lib.feather02();
	this.instance_9.setTransform(276.7,56.9,1,1,30,0,0,8.3,7.4);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.instance_9.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(10).to({_off:false},0).to({regY:7.3,rotation:0,x:298,y:50.3,alpha:1},10,cjs.Ease.get(1)).to({rotation:-23.2,x:300.9,y:50.4},4,cjs.Ease.get(1)).to({rotation:0,x:298,y:50.3},4,cjs.Ease.get(1)).wait(2));

	// feather01
	this.instance_10 = new lib.feather01();
	this.instance_10.setTransform(247.2,54.3,1,1,-30,0,0,5.1,5.5);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.instance_10.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(8).to({_off:false},0).to({regX:5.2,rotation:0,x:230,y:50,alpha:1},8,cjs.Ease.get(0.62)).to({regX:5.3,regY:5.6,rotation:45,x:224.7,y:51.4},5,cjs.Ease.get(1)).to({regX:5.2,regY:5.5,rotation:0,x:229.1,y:49.9},4,cjs.Ease.get(1)).wait(5));

	// lance
	this.instance_11 = new lib.lance();
	this.instance_11.setTransform(283.7,46.3,0.497,0.497,0,-12,168,40.4,10.4);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.instance_11.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(10).to({_off:false},0).to({x:301.3,y:38.8,alpha:1},11,cjs.Ease.get(1)).wait(9));

	// lance
	this.instance_12 = new lib.lance();
	this.instance_12.setTransform(237.7,46.3,0.497,0.497,12,0,0,40.4,10.4);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.instance_12.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(8).to({_off:false},0).to({regY:10.7,x:221.4,y:39.3,alpha:1},11,cjs.Ease.get(1)).wait(11));

	// circle_gradient copy 3
	this.instance_13 = new lib.circle_gradient();
	this.instance_13.setTransform(182.4,121.9,5.913,1.587,0,0,0,30.9,30.7);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(3).to({_off:false},0).to({alpha:0.602},14,cjs.Ease.get(1)).wait(13));

	// circle_gradient copy 2
	this.instance_14 = new lib.circle_gradient();
	this.instance_14.setTransform(168.6,59.3,5.465,1.587,0,0,0,30.9,30.7);
	this.instance_14.alpha = 0;
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(3).to({_off:false},0).to({alpha:0.602},14,cjs.Ease.get(1)).wait(13));

	// circle_gradient copy
	this.instance_15 = new lib.circle_gradient();
	this.instance_15.setTransform(445,62,6.985,1.758,0,0,0,30.9,30.7);
	this.instance_15.alpha = 0;
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(3).to({_off:false},0).to({alpha:0.602},14).wait(13));

	// circle_gradient
	this.instance_16 = new lib.circle_gradient();
	this.instance_16.setTransform(464,106.8,6.385,1.656,0,0,0,31,30.8);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(10).to({_off:false},0).to({alpha:0.59},11,cjs.Ease.get(1)).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(306,132,600.3,43.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;